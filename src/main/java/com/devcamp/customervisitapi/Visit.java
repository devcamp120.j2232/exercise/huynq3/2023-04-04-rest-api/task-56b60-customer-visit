package com.devcamp.customervisitapi;

import java.util.Date;

public class Visit extends Customer {
    private Customer customer;
    private Date date;
    private double serviceExpenses = 10000000;
    private double productExpenses = 2000000;
    
    public Visit(String name, Date date) {
        super(name);
        this.date = date;
    }
    
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getServiceExpenses() {
        return serviceExpenses;
    }
    public void setServiceExpenses(double serviceExpenses) {
        this.serviceExpenses = serviceExpenses;
    }
    public double getProductExpenses() {
        return productExpenses;
    }
    public void setProductExpenses(double productExpenses) {
        this.productExpenses = productExpenses;
    }
    public double getTotalExpense(){
        return serviceExpenses + productExpenses;
    }
    @Override
    public String toString() {
        return "Visit [customer=" + customer + ", date=" + date + ", serviceExpenses=" + serviceExpenses
                + ", productExpenses=" + productExpenses + "]";
    }
    
}
